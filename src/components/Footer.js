import { Container, Row, Col } from 'react-bootstrap';

const Footer = () => {
    return (
        <Container fluid className="mt-5">
            <Row>
                <Col>
                    <strong>React</strong> Footer
                </Col>
            </Row>
        </Container>
            
    );
}

export default Footer;