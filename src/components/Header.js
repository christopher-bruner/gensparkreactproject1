import { NavLink } from 'react-router-dom';

const Header = () => {

    return (
        <>
        <nav className="navbar navbar-expand navbar-dark bg-dark">\
        <div className="container-fluid">
            <div className="navbar-brand">News Site</div>
            <div className="collapse navbar-collapse">
                <ul className="navbar-nav me-auto">
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/">Home</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/contact">Contact Us</NavLink>
                    </li>
               </ul>
            </div>
            <div className="col-4 d-flex justify-content-end">
                <NavLink className="btn btn-outline-secondary" to="/create">Create</NavLink>
            </div>
        </div>
    </nav>
    </>
    )

}

export default Header;