import { NavLink, useParams, Outlet } from 'react-router-dom';
import { getArticle } from '../articles';

export const ArticlePreview = ({ article }) => {

    return (
        article ? (
            <article className="blog-post">
                {/* <h2 className="blog-post-title mb-1">{article.title}</h2> */}
                            <NavLink to={`/article/${article.id}`} as="h1" className="display-4 text-nowrap">{article.title}</NavLink>
                <p className="blog-post-meta mb-1">{article.date} by <NavLink to={`/author/${article.author.toLowerCase()}`}>Bill</NavLink></p>
                <p>{article.content}</p>
                <hr />
            </article>
        ) : null
    )

}

export const ArticleSpotlight = ({ article }) => {

    return (
        article ? (
            <div className="p-4 p-md-5 mb-4 rounded text-bg-dark">
                <div className="col-md-6 px-0">
                    <h1 className="display-4 font-italic">{article.title}</h1>
                    <p className="lead my-3">{article.content}</p>
                    <p className="lead mb-0">
                        <NavLink to={`/article/${article.id}`} className="text-white font-weight-bold">Continue reading...</NavLink>
                    </p>
                </div>
            </div>
        ) : null
    )

}

export const Article = () => {

    let params = useParams();
    let article = getArticle(parseInt(params.id));

    return (
        article ? (

            <main className="container mt-5">

                <div className="col-md-8">

                    <div className="p-4 mt-4 mb-4 text-bg-secondary position-absolute top-50 start-50 translate-middle">
                        <div className="col-md-6 mt-1">
                            <h1 className="display-4 text-nowrap">{article.title}</h1>
                            <img src="/img/placeimg_500_200_tech.jpg" alt="Enim laboris" className="float-start" />
                                <p className='blog-post-meta mb-1'>{article.content}</p>
                        </div>

                    </div>

                </div>

            </main>

        ) : null
    )

}

export const Articles = () => {

    return (
        <>
        <Outlet />
        </>
    )

}