export const Contact = () => {

    return (
        
    <main className="container mt-5">

    <div className=" text-center mt-5 ">
        <h1>Contact Form</h1>
    </div>

    <div className="row ">
        <div className="col-lg-7 mx-auto">
            <div className="card mt-2 mx-auto p-4 text-bg-secondary">
                <div className="card-body rounded text-bg-light">

                    <div className="container">
                        <form id="contact-form" role="form">

                            <div className="controls">

                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="firstname">Firstname *</label>
                                            <input id="firstname" type="text" name="firstname" className="form-control"
                                                placeholder="Please enter your firstname *" />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="lastname">Lastname *</label>
                                            <input id="lastname" type="text" name="lastname" className="form-control"
                                                placeholder="Please enter your lastname *" />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="email">Email *</label>
                                            <input id="email" type="email" name="email" className="form-control"
                                                placeholder="Please enter your email *" />

                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="phone">Phone *</label>
                                            <input id="phone" type="text" name="phone" className="form-control"
                                                placeholder="Please enter your phone number *" />

                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="form-group">
                                            <label htmlFor="message">Message *</label>
                                            <textarea id="message" name="message" maxLength="250" className="form-control"
                                                placeholder="Write your message here." rows="4" ></textarea>
                                        </div>
                                    </div>

                                    <div className="col-md-12 mt-4">
                                        <input type="submit" className="btn btn-success btn-send  pt-2 btn-block"
                                            value="Send Message" />
                                    </div>

                                </div>

                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>

</main>


    )
}