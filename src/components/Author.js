import { useParams, Outlet } from 'react-router-dom';

import { getAuthor } from '../authors';

export const Author = () => {

    let params = useParams();
    let author = getAuthor(params.id);

    return (
        author ? (
            <main className="container mt-5">

                <div className="p-4 p-md-5 mb-4 rounded text-bg-dark">
                    <div className="row flex-v-align">
                        <div className="col-sm-12 col-md-5 col-lg-5">
                            {/* <img src="/img/1024x1024.jpg" className="img-fluid rounded-circle" alt="Bill Murray" /> */}
                            <img src={author.image} className="img-fluid rounded-circle" alt={author.name} />
                        </div>
                        <div className="col-sm-12 col-md-7 col-lg-7">
                            <div className="hp-left">
                                <h1>{author.name}</h1>
                                <p>{author.content}</p>
                                <div className="hp-left-bottom">
                                    <a href="#"><i className="bi bi-twitter"></i></a>
                                    <a href="#"><i className="bi bi-facebook"></i></a>
                                    <a href="#"><i className="bi bi-instagram"></i></a>
                                    <a href="#"><i className="bi bi-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </main>
        ) : null
    )

}

export const Authors = () => {

    return (
        <>
        <Outlet />
        </>
    )

}