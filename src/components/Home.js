import { useEffect, useState } from "react";
import { Outlet } from "react-router-dom";

import { getArticles } from "../articles";
import { ArticlePreview, ArticleSpotlight } from "./Article";

const Home = () => {

    const [articles, setArticles] = useState([]);
    const [spotlightArticle, setSpotlightArticle] = useState(null);
    const [previewArticles, setPreviewArticles] = useState([]);

    useEffect(() => {

        if (articles.length === 0) {

            const a = getArticles();
            setArticles(a);

            setSpotlightArticle(a[0]);

            const p = a.slice(1);
            setPreviewArticles(p);

        }

    }, [articles]);

    return articles.length > 0 ? (

        <main className="container mt-5">

            <ArticleSpotlight article={spotlightArticle} />

            <div className="col-md-8">

                {
                    previewArticles.length > 0 ?
                    previewArticles.map(article => (
                        <ArticlePreview key={article.id} article={article} />
                    )) : null
                }

            </div>

            <Outlet />

        </main>

            ) : (
                <div className="container mt-5">

                    <div className="row">
                        <div className="col-md-8">
                            <h1>Loading...</h1>
                        </div>
                    </div>
                </div>
            )

}

export default Home;