export const Create = () => {

    return (

        <main className="container mt-5 rounded text-bg-secondary">

            <div className="mb-3">
                <label htmlFor="exampleFormControlInput1" className="form-label mt-3">Article Title</label>
                <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="title..." />
            </div>
            <div className="mb-3">
                <label htmlFor="exampleFormControlTextarea1" className="form-label">Content</label>
                <textarea className="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
            <input className="btn btn-primary mb-3" type="submit" value="Submit" />

        </main>
    )

}