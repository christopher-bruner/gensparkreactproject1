import { useEffect, useState } from 'react';
import { Form, Row, Col, Button, Alert } from 'react-bootstrap';

const SignupForm = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [fname, setFname] = useState('');
    const [lname, setLname] = useState('');
    const [phone, setPhone] = useState('');

    const [isSubmitting, setIsSubmitting] = useState(false);
    const [isSuccess, setIsSuccess] = useState(false);
    const [isError, setIsError] = useState(false);

    useEffect(() => {

        if (isSubmitting) {

            if (email && password && fname && lname && phone) {

                if (isEmailValid(email) && isPhoneValid(phone) && isPasswordValid(password)) setIsSuccess(true);
                else setIsError(true);

            } else setIsError(true);

            setIsSubmitting(false);

        }

     }, [email, password, fname, lname, phone, isSubmitting]);

    const isEmailValid = email => {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    const isPhoneValid = phone => {

        const re = /^\d{10}$/;
        return re.test(phone);

    }

    const isPasswordValid = password => {

        const re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
        return re.test(password);

    }

    return (

        <>

            {
                isSuccess && <Alert variant="success"> Successfully signed up! </Alert>
            }
            {
                isError && <Alert variant="danger"> Error signing up! </Alert>
            }

            <Form className="mt-5">

                <Row className="mb-3">
                    <Form.Group as={Col}>
                        <Form.Label>Email</Form.Label>
                        <Form.Control onChange={setEmail} placeholder="Email" type="email" />
                    </Form.Group>
                    <Form.Group as={Col}>
                        <Form.Label>Password</Form.Label>
                        <Form.Control onChange={setPassword} placeholder="Password" type="password" />
                    </Form.Group>
                </Row>

                <Row className="mb-3">
                    <Form.Group as={Col}>
                        <Form.Label>First Name</Form.Label>
                        <Form.Control onChange={setFname} placeholder="First Name" />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control onChange={setLname} placeholder="Last Name" />
                    </Form.Group>
                </Row>

                <Row className="mb-3">
                    <Form.Group as={Col}>
                        <Form.Label>Phone</Form.Label>
                        <Form.Control onChange={setPhone} placeholder="Phone Number" />
                    </Form.Group>
                </Row>

                <Button variant="primary" type="submit" disabled={isSubmitting} onClick={() => setIsSubmitting(true)}>Sign Up</Button>

            </Form>
        </>
    )

}

export default SignupForm;