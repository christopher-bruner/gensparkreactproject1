let articles = [

    {
        id: 1,
        title: "Enim laboris",
        date: "August 4, 2022",
        author: "Bill",
        content: "Minim qui eiusmod cillum nisi ut ea ad commodo velit cillum minim excepteur cupidatat cupidatat. Qui incididunt fugiat deserunt nostrud ad cillum exercitation sint esse ea fugiat dolor dolor eiusmod. Anim deserunt eiusmod duis proident consequat. Consequat dolore id fugiat labore pariatur velit. Nulla labore occaecat ipsum fugiat nostrud.",
    },
    {
        id: 2,
        title: "Ex dolor cillum id est",
        date: "August 1, 2022",
        author: "Bill",
        content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate, quidem, asperiores, quisquam, aliquam, minus, mollitia, quas, blanditiis, sapiente, inventore, maxime.",
    },
    {
        id: 3,
        title: "Cillum fugiat exercitation ullamco",
        date: "March 14, 2022",
        author: "Bill",
        content: "Proident laborum laborum aliqua sint irure in. In laboris aliquip reprehenderit esse dolor ea amet eu ex incididunt adipisicing duis deserunt eu. Incididunt non irure pariatur fugiat consequat veniam nisi velit cillum ullamco. Sunt incididunt anim duis in irure et nostrud pariatur ex elit labore est tempor cillum. In pariatur ex veniam velit ullamco reprehenderit.",
    },
    {
        id: 4,
        title: "Do ullamco laborum aliquip",
        date: "January 12, 2022",
        author: "Bill",
        content: "Ad quis mollit enim ea duis id ex velit proident. Sint excepteur reprehenderit sunt incididunt consectetur ullamco sunt enim qui et minim anim ad ex. Est minim nulla esse nostrud quis. Reprehenderit aliquip dolor laborum enim cupidatat labore velit. Sint elit consequat fugiat cupidatat eiusmod ex occaecat. Sunt sint sunt elit veniam nostrud eu exercitation duis irure. Nulla est minim Lorem aliquip aliquip.",
    },
];

export function getArticles() {
    return articles;
}

export function getArticle(id) {
    return articles.find(article => article.id === id);
}