const authors = [
    {
        id: 'bill',
        name: 'Bill Murray',
        image: '/img/bill-murray.jpg',
        content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, quidem, temporibus, nisi, aliquam, optio, provident, amet, accusantium, error, voluptas, quae, iste. Quisquam, quidem, temporibus, nisi, aliquam, optio, provident, amet, accusantium, error, voluptas, quae, iste.'
    },
];

export function getAuthors() {
    return authors;
}

export function getAuthor(id) {
    return authors.find(author => author.id === id);
}