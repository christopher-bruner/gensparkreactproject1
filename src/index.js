import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import './index.css';
import App from './App';
import SignupForm from './components/SignupForm';
import Home from './components/Home';
import { Article, Articles } from './components/Article';
import { Author, Authors } from './components/Author';
import { Contact } from './components/Contact';
import { Create } from './components/Create';
// import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Router>
    <Routes>
      <Route path="/" element={<App />}>
        <Route index element={<Home />} />
        <Route path="contact" element={<Contact />} />
        <Route path="create" element={<Create />} />
        <Route path="article" element={<Articles />}>
          <Route path=":id" element={<Article />} />
        </Route>
        <Route path="author" element={<Authors />}>
          <Route path=":id" element={<Author />} />
        </Route>
        <Route path="signup" element={<SignupForm />} />
      </Route>
      <Route path="*" element={<div>Not Found</div>} />
    </Routes>
  </Router>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();